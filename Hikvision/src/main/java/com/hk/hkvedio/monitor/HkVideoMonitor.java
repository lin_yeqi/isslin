package com.hk.hkvedio.monitor;

import com.alibaba.fastjson.JSONObject;
import com.hk.hkvedio.utils.HCNetSDK;
import com.sun.jna.NativeLong;
import com.sun.jna.Pointer;

import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;

public class HkVideoMonitor implements HCNetSDK.FMSGCallBack {

    @Override
    public void invoke(NativeLong lCommand, HCNetSDK.NET_DVR_ALARMER pAlarmer, HCNetSDK.RECV_ALARM pAlarmInfo, int dwBufLen, Pointer pUser) {
        System.out.println("正在监听......");
        String sAlarmType = new String();
        String[] newRow = new String[3];
        String[] sIP = new String[2];
        //lCommand是传的报警类型
        System.out.println("警报编码：" + lCommand.intValue());
        switch (lCommand.intValue()) {
            //9000报警信息主动上传
            case HCNetSDK.COMM_ALARM_V30:
                System.out.println("9000报警");
                HCNetSDK.NET_DVR_ALARMINFO_V30 strAlarmInfoV30 = new HCNetSDK.NET_DVR_ALARMINFO_V30();
                strAlarmInfoV30.write();
                Pointer pInfoV30 = strAlarmInfoV30.getPointer();
                pInfoV30.write(0, pAlarmInfo.RecvBuffer, 0, strAlarmInfoV30.size());
                strAlarmInfoV30.read();
                switch (strAlarmInfoV30.dwAlarmType) {
                    case 0:
                        sAlarmType = new String("信号量报警");
                        break;
                    case 1:
                        sAlarmType = new String("硬盘满");
                        break;
                    case 2:
                        sAlarmType = new String("信号丢失");
                        break;
                    case 3:
                        sAlarmType = new String("移动侦测");
                        System.out.println("警告：移动侦测");
                        System.out.println("警告：移动侦测");
                        System.out.println("警告：移动侦测");
                        System.out.println("警告：移动侦测");
                        System.out.println("警告：移动侦测");
                        break;
                    case 4:
                        sAlarmType = new String("硬盘未格式化");
                        break;
                    case 5:
                        sAlarmType = new String("读写硬盘出错");
                        break;
                    case 6:
                        sAlarmType = new String("遮挡报警");
                        break;
                    case 7:
                        sAlarmType = new String("制式不匹配");
                        break;
                    case 8:
                        sAlarmType = new String("非法访问");
                        break;
                }
                newRow[0] = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd HH:mm:ss"));
                //报警类型
                newRow[1] = sAlarmType;
                //报警设备IP地址
                sIP = new String(pAlarmer.sDeviceIP).split("\0", 2);
                newRow[2] = sIP[0];
                break;
            //8000报警信息主动上传
            case HCNetSDK.COMM_ALARM:
                System.out.println("8000报警");
                HCNetSDK.NET_DVR_ALARMINFO strAlarmInfo = new HCNetSDK.NET_DVR_ALARMINFO();
                strAlarmInfo.write();
                Pointer pInfo = strAlarmInfo.getPointer();
                pInfo.write(0, pAlarmInfo.RecvBuffer, 0, strAlarmInfo.size());
                strAlarmInfo.read();
                switch (strAlarmInfo.dwAlarmType) {
                    case 0:
                        sAlarmType = new String("信号量报警");
                        break;
                    case 1:
                        sAlarmType = new String("硬盘满");
                        break;
                    case 2:
                        sAlarmType = new String("信号丢失");
                        break;
                    case 3:
                        sAlarmType = new String("移动侦测");
                        System.out.println("警告：移动侦测");
                        System.out.println("警告：移动侦测");
                        System.out.println("警告：移动侦测");
                        System.out.println("警告：移动侦测");
                        System.out.println("警告：移动侦测");
                        System.out.println("警告：移动侦测");
                        break;
                    case 4:
                        sAlarmType = new String("硬盘未格式化");
                        break;
                    case 5:
                        sAlarmType = new String("读写硬盘出错");
                        break;
                    case 6:
                        sAlarmType = new String("遮挡报警");
                        break;
                    case 7:
                        sAlarmType = new String("制式不匹配");
                        break;
                    case 8:
                        sAlarmType = new String("非法访问");
                        break;
                }

                newRow[0] = LocalDateTime.now().format(DateTimeFormatter.ofPattern("yyyy-MM-dd 00:00:00"));
                //报警类型
                newRow[1] = sAlarmType;
                //报警设备IP地址
                sIP = new String(pAlarmer.sDeviceIP).split("\0", 2);
                newRow[2] = sIP[0];
                break;

            //ATM DVR transaction information
            case HCNetSDK.COMM_TRADEINFO:
                //处理交易信息报警
                break;

            //IPC接入配置改变报警
            case HCNetSDK.COMM_IPCCFG:
                // 处理IPC报警
                break;

            default:
                System.out.println("未知报警类型");
                break;
        }
        System.out.println(JSONObject.toJSONString(newRow));

    }
}
