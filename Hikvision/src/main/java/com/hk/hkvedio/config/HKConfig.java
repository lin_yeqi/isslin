package com.hk.hkvedio.config;

import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Configuration;

@Configuration
public class HKConfig {

    /**
     * 库文件地址，绝对路径
     */
    public static String dllPath;

    /**
     * 下载地址,相对路径
     */
    public static String downloadPath;

    public static String ip;

    public static short port;

    public static String username;

    public static String password;

    /**
     * 文件大小低于这个值不下载
     */
    public static Integer fileMinSize;

    @Value("${diy-config.hk-video.dll-path}")
    private void setDllPath(String dllPath) {
        HKConfig.dllPath = dllPath;
    }

    @Value("${diy-config.hk-video.download-path}")
    private void setDownload(String downloadPath) {
        HKConfig.downloadPath = downloadPath;
    }

    @Value("${diy-config.hk-video.ip}")
    private void setIp(String ip) {
        HKConfig.ip = ip;
    }

    @Value("${diy-config.hk-video.port}")
    private void setPort(String port) {
        HKConfig.port = (short) Integer.parseInt(port);
    }

    @Value("${diy-config.hk-video.username}")
    private void setUsername(String username) {
        HKConfig.username = username;
    }

    @Value("${diy-config.hk-video.password}")
    private void setPassword(String password) {
        HKConfig.password = password;
    }

    @Value("${diy-config.hk-video.file-min-size}")
    private void setFileMinSize(String fileMinSize) {
        HKConfig.fileMinSize = Integer.parseInt(fileMinSize);
    }
}
