package com.hk.hkvedio;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@SpringBootApplication
public class HkVedioApplication {

    public static void main(String[] args) {
        SpringApplication.run(HkVedioApplication.class, args);
    }

}
