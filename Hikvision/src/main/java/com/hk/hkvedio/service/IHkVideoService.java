package com.hk.hkvedio.service;

import com.hk.hkvedio.utils.HCNetSDK;
import com.hk.hkvedio.vo.FileInfoVo;
import com.sun.jna.NativeLong;

import java.util.LinkedList;
import java.util.List;

public interface IHkVideoService {

    /**
     * 获取sdk 对象
     *
     * @return
     */
    HCNetSDK getHkSdk();

    /**
     * sdk初始化
     *
     * @return
     */
    HCNetSDK net_dvr_init();

    /**
     * 设备注册，V30接口
     *
     * @return
     */
    NativeLong net_dvr_login_v30();

    /**
     * 设备注册 V40
     *
     * @return
     */
    NativeLong net_dvr_login_v40();

    /**
     * NET_DVR_Login_V30 注册设备 获取通道号
     *
     * @return
     */
    List<Integer> createDeviceTreeV30();

    /**
     * NET_DVR_Login_V40 注册设备 获取通道号
     */
    LinkedList<Integer> createDeviceTreeV40();

    /**
     * 获取通道号名称
     *
     * @param chnanelNumber 通道号
     * @return
     */
    public String getChnanelName(Integer chnanelNumber);

    /**
     * 文件搜索
     *
     * @param startTime     开始时间
     * @param endTime       结束时间
     * @param channelNumber 通道号
     * @param fileType      录象文件类型0xff－全部，0－定时录像,1-移动侦测 ，2－报警触发，3-报警|移动侦测 4-报警&移动侦测 5-命令触发 6-手动录像
     * @return
     * @throws Exception
     */
    public List<FileInfoVo> findFileV30(HCNetSDK.NET_DVR_TIME startTime, HCNetSDK.NET_DVR_TIME endTime, Integer channelNumber, Integer fileType) throws Exception;

    /**
     * 文件搜索，V40
     *
     * @param startTime     开始时间
     * @param endTime       结束时间
     * @param channelNumber 通道号
     * @param fileType      录象文件类型0xff－全部，0－定时录像,1-移动侦测 ，2－报警触发，3-报警|移动侦测 4-报警&移动侦测 5-命令触发 6-手动录像
     * @return
     * @throws Exception
     */
    public List<FileInfoVo> findFileV40(HCNetSDK.NET_DVR_TIME startTime, HCNetSDK.NET_DVR_TIME endTime, Integer channelNumber, Integer fileType) throws Exception;


}
