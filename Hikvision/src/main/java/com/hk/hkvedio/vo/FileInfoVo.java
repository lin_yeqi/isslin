package com.hk.hkvedio.vo;

import java.io.Serializable;

public class FileInfoVo implements Serializable {

    /**
     * 文件大小，单位kb，转换成M,公式：fileSize / (1024 * 1024)
     */
    private Integer fileSize;

    /**
     * 文件名称
     */
    private String fileName;

    /**
     * 开始时间
     */
    private String fileBeginTime;

    /**
     * 结束时间
     */
    private String fileEndTime;

    /**
     * 设备编号
     */
    private String deviceId;

    /**
     * 年月日时间
     */
    private String yyyyMMdd;

    /**
     * 时分秒时间
     */
    private String hhmmss;

    public String getYyyyMMdd() {
        return yyyyMMdd;
    }

    public void setYyyyMMdd(String yyyyMMdd) {
        this.yyyyMMdd = yyyyMMdd;
    }

    public String getHhmmss() {
        return hhmmss;
    }

    public void setHhmmss(String hhmmss) {
        this.hhmmss = hhmmss;
    }

    public Integer getFileSize() {
        return fileSize;
    }

    public void setFileSize(Integer fileSize) {
        this.fileSize = fileSize;
    }

    public String getFileName() {
        return fileName;
    }

    public void setFileName(String fileName) {
        this.fileName = fileName;
    }

    public String getFileBeginTime() {
        return fileBeginTime;
    }

    public void setFileBeginTime(String fileBeginTime) {
        this.fileBeginTime = fileBeginTime;
    }

    public String getFileEndTime() {
        return fileEndTime;
    }

    public void setFileEndTime(String fileEndTime) {
        this.fileEndTime = fileEndTime;
    }

    public String getDeviceId() {
        return deviceId;
    }

    public void setDeviceId(String deviceId) {
        this.deviceId = deviceId;
    }
}
