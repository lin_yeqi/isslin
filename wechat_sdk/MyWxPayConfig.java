package org.jeecg.common.wechat.pay.config;

import org.jeecg.common.wechat.pay.sdk.IWXPayDomain;
import org.jeecg.common.wechat.pay.sdk.WXPayConfig;
import org.jeecg.common.wechat.pay.sdk.WXPayConstants;
import org.springframework.stereotype.Service;

import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;

/**
 * 继承微信sdk的配置类
 */
@Service
public class MyWxPayConfig extends WXPayConfig {

    private byte[] certData;
    private String appId = "wxac5825a2091d7919";

    private String wxPayKey = "XNoLzntDGLB6vddx98pjMWf3dUHGxMKp";

    private String wxPayMchId = "1600828265";

    /*private String certPath = "D:\\work\\mj_mall\\jeecg-cloud\\key\\apiclient_cert.p12";
    //linux路径
    //private String certPath = "/mnt/mall-service/pay/wechatKey/apiclient_cert.p12";*/

    private String linuxOS = "/mnt/mall-service/pay/wechatKey/apiclient_cert.p12";
    private String windowsOS = "D:\\work\\mj_mall\\jeecg-cloud\\key\\apiclient_cert.p12";

    @Override
    public String getAppID() {
        return appId;
    }

    @Override
    public String getMchID() {
        return wxPayMchId;
    }

    @Override
    public String getKey() {
        return wxPayKey;
    }

    @Override
    public InputStream getCertStream() {
        return new ByteArrayInputStream(this.certData);
    }

    @Override
    public IWXPayDomain getWXPayDomain() {
        IWXPayDomain iwxPayDomain = new IWXPayDomain() {
            @Override
            public void report(String domain, long elapsedTimeMillis, Exception ex) {

            }

            @Override
            public DomainInfo getDomain(WXPayConfig config) {
                return new IWXPayDomain.DomainInfo(WXPayConstants.DOMAIN_API, true);
            }
        };
        return iwxPayDomain;
    }

    /**
     * 构造方法读取证书, 通过getCertStream 可以使sdk获取到证书
     */
    public MyWxPayConfig() throws Exception {
        String property = System.getProperty("os.name");
        File file = null;
        if (property.contains("Windows") || property.contains("windows")) {
            file = new File(windowsOS);
        } else {
            file = new File(linuxOS);
        }
        InputStream certStream = new FileInputStream(file);
        this.certData = new byte[(int) file.length()];
        certStream.read(this.certData);
        certStream.close();
    }

}
