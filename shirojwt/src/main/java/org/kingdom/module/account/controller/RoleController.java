package org.kingdom.module.account.controller;


import org.springframework.web.bind.annotation.RequestMapping;

import org.springframework.web.bind.annotation.RestController;

/**
 * <p>
 * 角色 前端控制器
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
@RestController
@RequestMapping("/role")
public class RoleController {

}

