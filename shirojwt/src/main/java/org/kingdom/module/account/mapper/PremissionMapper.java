package org.kingdom.module.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.kingdom.module.account.entity.Premission;

import java.util.Set;

/**
 * <p>
 * 权限许可 Mapper 接口
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
public interface PremissionMapper extends BaseMapper<Premission> {

    public Set<String> listPremission(String roleId);

}
