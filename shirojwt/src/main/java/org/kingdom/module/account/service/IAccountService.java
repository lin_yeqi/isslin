package org.kingdom.module.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.kingdom.module.account.entity.Account;

/**
 * <p>
 * 用户 服务类
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
public interface IAccountService extends IService<Account> {

    public Account getAccountByUsername(String username);

}
