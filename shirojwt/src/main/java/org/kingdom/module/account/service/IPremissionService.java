package org.kingdom.module.account.service;

import com.baomidou.mybatisplus.extension.service.IService;
import org.kingdom.module.account.entity.Premission;

import java.util.Set;

/**
 * <p>
 * 权限许可 服务类
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
public interface IPremissionService extends IService<Premission> {

    public Set<String> listPremission(String roleId);

}
