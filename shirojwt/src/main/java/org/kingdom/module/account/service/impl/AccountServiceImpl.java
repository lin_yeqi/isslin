package org.kingdom.module.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.kingdom.module.account.entity.Account;
import org.kingdom.module.account.mapper.AccountMapper;
import org.kingdom.module.account.service.IAccountService;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户 服务实现类
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
@Service
public class AccountServiceImpl extends ServiceImpl<AccountMapper, Account> implements IAccountService {

    @Autowired
    private AccountMapper accountMapper;

    @Override
    public Account getAccountByUsername(String username) {
        return accountMapper.getAccountByUsername(username);
    }
}
