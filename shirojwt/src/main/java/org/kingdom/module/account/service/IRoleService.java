package org.kingdom.module.account.service;

import org.kingdom.module.account.entity.Role;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 * 角色 服务类
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
public interface IRoleService extends IService<Role> {

    public Role getAccountRole(String username);

}
