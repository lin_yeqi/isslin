package org.kingdom.module.account.service.impl;

import org.kingdom.module.account.entity.RolePremission;
import org.kingdom.module.account.mapper.RolePremissionMapper;
import org.kingdom.module.account.service.IRolePremissionService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
@Service
public class RolePremissionServiceImpl extends ServiceImpl<RolePremissionMapper, RolePremission> implements IRolePremissionService {

}
