package org.kingdom.module.account.service.impl;

import org.kingdom.module.account.entity.Role;
import org.kingdom.module.account.mapper.RoleMapper;
import org.kingdom.module.account.service.IRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 角色 服务实现类
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
@Service
public class RoleServiceImpl extends ServiceImpl<RoleMapper, Role> implements IRoleService {

    @Override
    public Role getAccountRole(String username) {
        return baseMapper.getAccountRole(username);
    }
}
