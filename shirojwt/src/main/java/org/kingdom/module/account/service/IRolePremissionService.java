package org.kingdom.module.account.service;

import org.kingdom.module.account.entity.RolePremission;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
public interface IRolePremissionService extends IService<RolePremission> {

}
