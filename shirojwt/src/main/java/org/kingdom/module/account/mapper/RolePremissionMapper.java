package org.kingdom.module.account.mapper;

import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.kingdom.module.account.entity.RolePremission;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
public interface RolePremissionMapper extends BaseMapper<RolePremission> {

}
