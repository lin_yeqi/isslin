package org.kingdom.module.account.controller;


import com.alibaba.fastjson.JSONObject;
import io.swagger.annotations.Api;
import org.apache.shiro.authz.annotation.Logical;
import org.apache.shiro.authz.annotation.RequiresPermissions;
import org.apache.shiro.authz.annotation.RequiresRoles;
import org.kingdom.module.account.service.IAccountService;
import org.kingdom.module.shiro.jwt.JwtUtil;
import org.kingdom.module.utils.CommonFlag;
import org.kingdom.module.utils.RedisUtil;
import org.kingdom.module.utils.Result;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.*;

import javax.annotation.Resource;

/**
 * <p>
 * 用户 前端控制器
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
@Api(tags = "用戶管理")
@RestController
@RequestMapping("/account")
public class AccountController {

    @Resource
    private IAccountService accountService;
    @Autowired
    private RedisUtil redisUtil;

    @PostMapping(value = "/login")
    public Result<Object> login(@RequestParam(name = "username") String username,
                                @RequestParam(name = "usernameCode") String usernameCode) {
        String token = JwtUtil.sign(username, usernameCode);
        // 设置token缓存有效时间
        redisUtil.set(CommonFlag.PREFIX_USER_TOKEN + token, token);
        redisUtil.expire(CommonFlag.PREFIX_USER_TOKEN + token, JwtUtil.EXPIRE_TIME*2 / 1000);

        JSONObject object = new JSONObject();
        object.put("token", token);
        object.put("info", username + " --> " + usernameCode);
        Result<Object> result = new Result<>();
        result.setMessage("登录成功");
        result.setResult(object);
        return result;
    }

    /**
     * 测试地址：http://localhost:9999/account/delAccount
     * 请求头Token：X-Access-Token
     */
    @GetMapping(value = "/delAccount")
    @RequiresPermissions(value = {"del"})
    @RequiresRoles(value = {"kd_admin"})
    public Result<Object> delAccount() {
        return Result.ok("删除角色成功");
    }

    /**
     * 测试地址：http://localhost:9999/account/addAccount
     * 请求头Token：X-Access-Token
     */
    @GetMapping(value = "/addAccount")
    @RequiresPermissions(value = {"add"})
    @RequiresRoles({"kd_admin"})
    public Result<Object> addAccount() {
        return Result.ok("添加角色成功");
    }

    /**
     * 测试地址：http://localhost:9999/account/delList
     * 请求头Token：X-Access-Token
     */
    @GetMapping(value = "/delList")
    @RequiresPermissions(value = {"del123"})
    @RequiresRoles(value = {"kd_admin"},logical = Logical.OR)
    public Result<Object> delList() {
        return Result.ok("批量删除角色成功");
    }

    /**
     * 测试地址：http://localhost:9999/account/addList
     * 请求头Token：X-Access-Token
     */
    @GetMapping(value = "/addList")
    @RequiresPermissions(value = {"add"})
    @RequiresRoles(value = {"kd_admin_000"})
    public Result<Object> addList() {
        return Result.ok("批量增加角色成功");
    }

}

