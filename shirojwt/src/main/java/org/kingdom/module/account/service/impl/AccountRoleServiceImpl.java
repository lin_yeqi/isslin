package org.kingdom.module.account.service.impl;

import org.kingdom.module.account.entity.AccountRole;
import org.kingdom.module.account.mapper.AccountRoleMapper;
import org.kingdom.module.account.service.IAccountRoleService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 * 用户角色 服务实现类
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
@Service
public class AccountRoleServiceImpl extends ServiceImpl<AccountRoleMapper, AccountRole> implements IAccountRoleService {

}
