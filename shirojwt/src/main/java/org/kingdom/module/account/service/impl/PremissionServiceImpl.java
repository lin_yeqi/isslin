package org.kingdom.module.account.service.impl;

import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.kingdom.module.account.entity.Premission;
import org.kingdom.module.account.mapper.PremissionMapper;
import org.kingdom.module.account.service.IPremissionService;
import org.springframework.stereotype.Service;

import java.util.Set;

/**
 * <p>
 * 权限许可 服务实现类
 * </p>
 *
 * @author 书小页
 * @since 2020-09-05
 */
@Service
public class PremissionServiceImpl extends ServiceImpl<PremissionMapper, Premission> implements IPremissionService {

    @Override
    public Set<String> listPremission(String roleId) {
        return baseMapper.listPremission(roleId);
    }
}
