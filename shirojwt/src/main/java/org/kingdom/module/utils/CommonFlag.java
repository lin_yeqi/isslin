package org.kingdom.module.utils;

public interface CommonFlag {

    public static final String FLAG_0 = "0";
    public static final String FLAG_1 = "1";
    public static final String FLAG_TRUE = "true";
    public static final String FLAG_FALSE = "false";

    public final static String X_ACCESS_TOKEN = "X-Access-Token";
    public final static String TENANT_ID = "tenant_id";

    /** 登录用户Shiro权限缓存KEY前缀 */
    public static String PREFIX_USER_SHIRO_CACHE  = "shiro:cache:org.jeecg.modules.shiro.authc.ShiroRealm.authorizationCache:";
    /** 登录用户Token令牌缓存KEY前缀 */
    public static final String PREFIX_USER_TOKEN  = "prefix_user_token_";

    public static final Integer SC_OK_200  = 200;
    public static final Integer SC_INTERNAL_SERVER_ERROR_500 = 500;
    public static final Integer SC_JEECG_NO_AUTHZ=510;

}
