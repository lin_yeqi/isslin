package org.kingdom;

import org.mybatis.spring.annotation.MapperScan;
import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;

@MapperScan("org.kingdom.module.account.mapper")
@SpringBootApplication
public class ShirojwtApplication {

	public static void main(String[] args) {
		SpringApplication.run(ShirojwtApplication.class, args);
	}

}
