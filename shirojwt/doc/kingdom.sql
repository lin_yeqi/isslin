-- MySQL dump 10.13  Distrib 8.0.21, for Win64 (x86_64)
--
-- Host: 116.62.247.145    Database: kingdom
-- ------------------------------------------------------
-- Server version	8.0.21

/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!50503 SET NAMES utf8mb4 */;
/*!40103 SET @OLD_TIME_ZONE=@@TIME_ZONE */;
/*!40103 SET TIME_ZONE='+00:00' */;
/*!40014 SET @OLD_UNIQUE_CHECKS=@@UNIQUE_CHECKS, UNIQUE_CHECKS=0 */;
/*!40014 SET @OLD_FOREIGN_KEY_CHECKS=@@FOREIGN_KEY_CHECKS, FOREIGN_KEY_CHECKS=0 */;
/*!40101 SET @OLD_SQL_MODE=@@SQL_MODE, SQL_MODE='NO_AUTO_VALUE_ON_ZERO' */;
/*!40111 SET @OLD_SQL_NOTES=@@SQL_NOTES, SQL_NOTES=0 */;

--
-- Table structure for table `kd_account`
--

DROP TABLE IF EXISTS `kd_account`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kd_account` (
  `id` varchar(64) NOT NULL COMMENT '用户id',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '修改时间',
  `username` varchar(64) DEFAULT NULL COMMENT '登录账号',
  `username_code` varchar(64) DEFAULT NULL COMMENT '密码',
  `salt` varchar(64) DEFAULT NULL COMMENT '盐',
  `nick_name` varchar(64) DEFAULT NULL COMMENT '昵称',
  `phone` varchar(15) DEFAULT NULL COMMENT '手机',
  `birthday` datetime DEFAULT NULL COMMENT '生日',
  `sex` varchar(1) DEFAULT NULL COMMENT '性别',
  `mail` varchar(64) DEFAULT NULL COMMENT '邮箱',
  `remark` text COMMENT '备注',
  `account_status` varchar(1) DEFAULT '0' COMMENT '用户状态，0正常，1冻结',
  `del_flag` varchar(1) DEFAULT '0' COMMENT '逻辑删除，0正常，1删除',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kd_account`
--

LOCK TABLES `kd_account` WRITE;
/*!40000 ALTER TABLE `kd_account` DISABLE KEYS */;
INSERT INTO `kd_account` VALUES ('12313123',NULL,NULL,'1111',NULL,'12313','1231',NULL,NULL,NULL,NULL,NULL,NULL,NULL,'0','0');
/*!40000 ALTER TABLE `kd_account` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kd_account_role`
--

DROP TABLE IF EXISTS `kd_account_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kd_account_role` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `account_id` varchar(64) DEFAULT NULL COMMENT '用户id',
  `role_id` varchar(64) DEFAULT NULL COMMENT '角色id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='用户角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kd_account_role`
--

LOCK TABLES `kd_account_role` WRITE;
/*!40000 ALTER TABLE `kd_account_role` DISABLE KEYS */;
INSERT INTO `kd_account_role` VALUES ('123456789\r\n',NULL,NULL,'12313123','1231313222');
/*!40000 ALTER TABLE `kd_account_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kd_premission`
--

DROP TABLE IF EXISTS `kd_premission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kd_premission` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `update_by` varchar(64) DEFAULT NULL COMMENT '修改人',
  `update_time` datetime DEFAULT NULL COMMENT '更新时间',
  `premission_code` varchar(64) DEFAULT NULL COMMENT '权限编码',
  `premission_name` varchar(64) DEFAULT NULL COMMENT '权限名称',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='权限许可';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kd_premission`
--

LOCK TABLES `kd_premission` WRITE;
/*!40000 ALTER TABLE `kd_premission` DISABLE KEYS */;
INSERT INTO `kd_premission` VALUES ('111111111111',NULL,NULL,NULL,NULL,'add','新增'),('222222222222',NULL,NULL,NULL,NULL,'del','删除');
/*!40000 ALTER TABLE `kd_premission` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kd_role`
--

DROP TABLE IF EXISTS `kd_role`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kd_role` (
  `id` varchar(64) NOT NULL COMMENT '角色id',
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `role_code` varchar(64) DEFAULT NULL COMMENT '角色编码',
  `role_name` varchar(64) DEFAULT NULL COMMENT '角色名称',
  `remark` text COMMENT '备注',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci COMMENT='角色';
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kd_role`
--

LOCK TABLES `kd_role` WRITE;
/*!40000 ALTER TABLE `kd_role` DISABLE KEYS */;
INSERT INTO `kd_role` VALUES ('1231313222',NULL,NULL,'kd_admin','超级管理员',NULL);
/*!40000 ALTER TABLE `kd_role` ENABLE KEYS */;
UNLOCK TABLES;

--
-- Table structure for table `kd_role_premission`
--

DROP TABLE IF EXISTS `kd_role_premission`;
/*!40101 SET @saved_cs_client     = @@character_set_client */;
/*!50503 SET character_set_client = utf8mb4 */;
CREATE TABLE `kd_role_premission` (
  `id` varchar(64) NOT NULL,
  `create_by` varchar(64) DEFAULT NULL COMMENT '创建人',
  `create_time` datetime DEFAULT NULL COMMENT '创建时间',
  `role_id` varchar(64) DEFAULT NULL COMMENT '角色id',
  `premission_id` varchar(64) DEFAULT NULL COMMENT '权限id',
  PRIMARY KEY (`id`)
) ENGINE=InnoDB DEFAULT CHARSET=utf8mb4 COLLATE=utf8mb4_0900_ai_ci;
/*!40101 SET character_set_client = @saved_cs_client */;

--
-- Dumping data for table `kd_role_premission`
--

LOCK TABLES `kd_role_premission` WRITE;
/*!40000 ALTER TABLE `kd_role_premission` DISABLE KEYS */;
INSERT INTO `kd_role_premission` VALUES ('123123',NULL,NULL,'1231313222','111111111111'),('132222',NULL,NULL,'1231313222','222222222222');
/*!40000 ALTER TABLE `kd_role_premission` ENABLE KEYS */;
UNLOCK TABLES;
/*!40103 SET TIME_ZONE=@OLD_TIME_ZONE */;

/*!40101 SET SQL_MODE=@OLD_SQL_MODE */;
/*!40014 SET FOREIGN_KEY_CHECKS=@OLD_FOREIGN_KEY_CHECKS */;
/*!40014 SET UNIQUE_CHECKS=@OLD_UNIQUE_CHECKS */;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
/*!40111 SET SQL_NOTES=@OLD_SQL_NOTES */;

-- Dump completed on 2020-09-06 11:12:31
