package org.kingdom.utils.helper;

import lombok.extern.slf4j.Slf4j;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

@Slf4j
public class DateHelper {

    public  static Date NumberToDate(String number)
    {
        Calendar c=Calendar.getInstance();
        long seconds = Integer.parseUnsignedInt(number);
        long millions=new Long(seconds).longValue()*1000;
        c.setTimeInMillis(millions);
        return  c.getTime();
    }

    /**
     * "yyyy-MM-dd HH:mm:ss"
     * @param number
     * @return
     */
    public static String NumberToDateFormat(String number)
    {
        SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd HH:mm:ss");
        return  sdf.format(NumberToDate(number));
    }

    /**
     * 返回时间差
     * @param endDate
     * @param nowDate
     * @return
     */
    public static String getDateCompare(Date endDate, Date nowDate) {
        long nd = 1000 * 24 * 60 * 60;
        long nh = 1000 * 60 * 60;
        long nm = 1000 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - nowDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        // 计算差多少小时
        long hour = diff % nd / nh;
        // 计算差多少分钟
        long min = diff % nd % nh / nm;
        // 计算差多少秒//输出结果
        // long sec = diff % nd % nh % nm / ns;
        return day + "天" + hour + "小时" + min + "分钟";
    }

    /**
     * 返回天数差值
     * @param endDate
     * @param beginDate
     * @return
     */
    public static long getDayCompare(Date endDate, Date beginDate) {
        long nd = 1000 * 24 * 60 * 60;
        // long ns = 1000;
        // 获得两个时间的毫秒时间差异
        long diff = endDate.getTime() - beginDate.getTime();
        // 计算差多少天
        long day = diff / nd;
        return day;

    }

    public static long  getDayCompareAbs(Date d1,Date d2)
    {
        return Math.abs(getDayCompare(d1,d2));
    }

    /**
     * 返回月数差值
     * @param endDate
     * @param beginDate
     * @return
     */
    public static int getMonthPoorer(Date endDate, Date beginDate) {

        int result=0;
        Calendar endCalendar = Calendar.getInstance();
        endCalendar.setTime(endDate);
        Calendar beginCalendar = Calendar.getInstance();
        beginCalendar.setTime(endDate);

        result = endCalendar.get(Calendar.MONTH) - beginCalendar.get(Calendar.MONTH);

        return result;
    }

    public static long  getMonthCompareAbs(Date d1,Date d2)
    {
        return Math.abs(getMonthCompare(d1,d2));
    }

    /**
     * 返回计算的日期yyyy-MM-dd
     * @param date
     * @param dayNumber
     * @return
     */
    public static String getDateSdf(Date date,int dayNumber)
    {
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM-dd");
        return  df.format(new Date(date.getTime() - dayNumber * 24 * 60 * 60 * 1000));
    }

    /**
     * 返回计算的日期yyyyMMdd
     * @param date
     * @param dayNumber
     * @return
     */
    public static Integer getDateIdf(Date date,int dayNumber) {
        Integer dateInt = 0;
            SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
            dateInt = Integer.parseInt(df.format(new Date(date.getTime() + dayNumber * 24 * 60 * 60 * 1000)));

        return dateInt;

    }

    /**
     * 返回计算的日期yyyyMMdd
     * @param date
     * @return
     */
    public static Integer getDateIdf(Date date) {
        Integer dateInt = 0;
        SimpleDateFormat df = new SimpleDateFormat("yyyyMMdd");
        dateInt = Integer.parseInt(df.format(date));
        return dateInt;

    }

    /**
     * 返回计算的日期yyyyMM
     * @param date
     * @return
     */
    public static Integer getMonthIdf(Date date) {
        Integer dateInt = 0;
        SimpleDateFormat df = new SimpleDateFormat("yyyyMM");
        dateInt = Integer.parseInt(df.format(date));
        return dateInt;

    }

    /**
     * 返回计算的日期yyyyMM
     * @param date
     * @return
     */
    public static Integer getMonthIdf(Date date,int monthNumber) {
        Integer dateInt = 0;
        Date dateNew=getYearMonth(date,monthNumber);
        SimpleDateFormat df = new SimpleDateFormat("yyyyMM");
        dateInt = Integer.parseInt(df.format(dateNew));
        return dateInt;

    }

    /**
     * 返回计算的日期yyyy-MM
     * @param date
     * @param monthNumber
     * @return
     */
    public static String getMonthSdf(Date date,int monthNumber)
    {
        SimpleDateFormat df=new SimpleDateFormat("yyyy-MM");
        Date yearMonth=getYearMonth(date,monthNumber);
        return  df.format(yearMonth);
    }

    /**
     * 返回计算的日期
     * @param date yyyyMMdd  int
     * @return
     */
    public static Date getDate(int date) {
        Date d = null;
        String dateStr=String.valueOf(date);
        try {
            if(dateStr.length()==8)
            {

                int year=Integer.parseInt(dateStr.substring(0,4));
                int month=Integer.parseInt(dateStr.substring(4,2));
                int day=Integer.parseInt(dateStr.substring(6,2));
                Calendar calendar = Calendar.getInstance();
                calendar.set(year,month,day);
                d=calendar.getTime();

            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return d;
    }

    /**
     * 返回计算的日期
     * @param date yyyyMM  int
     * @return
     */
    public static Date getYearMonth(int date) {
        Date d = null;
        String dateStr=String.valueOf(date);
        try {
            if(dateStr.length()==6)
            {

                int year=Integer.parseInt(dateStr.substring(0,4));
                int month=Integer.parseInt(dateStr.substring(4,2));
                Calendar calendar = Calendar.getInstance();
                calendar.set(year,month);
                d=calendar.getTime();

            }
        } catch (Exception ex) {
            log.info(ex.getMessage());
        }
        return d;
    }

    /**
     * 返回计算的日期
     * @param date yyyyMMdd  int
     * @return
     */
    public static Date getDate(Date date,int days) {

        if(date!=null)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.DAY_OF_YEAR,days);
            Date d =calendar.getTime();
            return d;
        }
        else
        {
            return null;
        }
    }

    /**
     * 返回计算的月份
     * @param date yyyyMM  int
     * @return
     */
    public static Date getYearMonth(Date date,int months) {

        if(date!=null)
        {
            Calendar calendar = Calendar.getInstance();
            calendar.setTime(date);
            calendar.add(Calendar.MONTH,months);
            Date d =calendar.getTime();
            return d;
        }
        else
        {
            return null;
        }
    }

    /**
     * 返回按月交租金的月数
     * @param beginDate
     * @param currentDate
     * @return
     */
    public static int getMonthCompare(Date beginDate,Date currentDate) {

        Calendar currentCalendar = Calendar.getInstance();
        currentCalendar.setTime(currentDate);
        Calendar beginCalendar = Calendar.getInstance();
        beginCalendar.setTime(beginDate);
        int val=currentCalendar.get(Calendar.MONTH) - beginCalendar.get(Calendar.MONTH);
        val -=1;
        if(currentDate.getDay()>beginDate.getDay())
        {
            val+=1;
        }
        return val;
    }

    /**
     * @return返回微秒
     */
    public static Long getmicTime() {
        Long cutime = System.currentTimeMillis() * 1000; // 微秒
        Long nanoTime = System.nanoTime(); // 纳秒
        return cutime + (nanoTime - nanoTime / 1000000 * 1000000) / 1000;
    }


    /**
     * @return返回毫秒
     */
    public static Long getTimeMillis() {
        Long cutime = System.currentTimeMillis() ; // 毫秒
        return cutime;
    }
}
