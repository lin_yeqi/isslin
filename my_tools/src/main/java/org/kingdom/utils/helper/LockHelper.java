package org.kingdom.utils.helper;

import lombok.extern.slf4j.Slf4j;
import org.kingdom.utils.Result;
import org.springframework.stereotype.Controller;

import java.util.HashMap;
import java.util.LinkedList;
import java.util.concurrent.locks.Lock;
import java.util.concurrent.locks.ReentrantLock;

/**
 * 锁
 */
@Slf4j
@Controller
public  class LockHelper {

    private static LinkedList<String> lockList=new LinkedList<>();
    private static HashMap<String,Boolean> lockMap=new HashMap<String,Boolean>();

    public static Lock lockUpdateCount = new ReentrantLock(false);

    public static Lock lockBalance = new ReentrantLock(false);
    /**
     * 新增
     * @param key
     * @return
     */
    public static boolean addLockKey(String key)
    {
        try {
            boolean  bl= lockList.add(key);
            return bl;
        }
        catch (Exception e)
        {
            return  false;
        }
    }

    /**
     * 新增
     * @param key
     * @return
     */
    public static boolean addLockMapKey(String key)
    {
        try {
            lockMap.put(key,false);
            return true;
        }
        catch (Exception e)
        {
            return  false;
        }
    }

    /**
     * 是否存在key
     * @param key
     * @return
     */

    public static boolean isExist(String key)
    {
        try {
            boolean  bl=  lockList.indexOf(key)==-1?false:true;
            return bl;
        }
        catch (Exception e)
        {
            return  false;
        }
    }

    /**
     * 是否存在key
     * @param key
     * @return
     */

    public static boolean isExistMap(String key)
    {
        try {
            Boolean  bl=  lockMap.get(key);
            if(bl==null)
            {
                return false;
            }
            else {
                return true;
            }
        }
        catch (Exception e)
        {
            return  false;
        }
    }


    /**
     * 是否存在LockKey,不存在添加，存在返回失败信息
     * @param key
     * @return
     */
    public static Result<Object> isExistOrAddLockKey(String key, String errorMessage)
    {
        Result<Object> result=new Result<>();
        try {
            if(LockHelper.isExist(key))
            {
                return result.error500(errorMessage);
            }
            if(LockHelper.addLockKey(key))
            {
                result.setSuccess(true);
            }
            else {
                return result.error500(errorMessage);
            }
        }
        catch (Exception e)
        {
            return result.error500(e.getMessage());
        }
        return result;
    }

    /**
     * 是否存在LockKey,不存在添加，存在返回失败信息
     * @param key
     * @return
     */
    public static Result<Object> isExistMapOrAddLockKey(String key,String errorMessage)
    {
        Result<Object> result=new Result<>();
        try {
            if(LockHelper.isExistMap(key))
            {
                return result.error500(errorMessage);
            }
            if(LockHelper.addLockMapKey(key))
            {
                result.setSuccess(true);
            }
            else {
                return result.error500(errorMessage);
            }
        }
        catch (Exception e)
        {
            return result.error500(e.getMessage());
        }
        return result;
    }

    /**
     * 移除
     * @param key
     * @return
     */
    public static boolean remove(String key)
    {
        try {
            boolean  bl= lockList.remove(key);
            log.info("remove"+key+" "+bl);
            return bl;
        }
        catch (Exception e)
        {
            return  false;
        }
    }

    /**
     * 是否存在key
     * @param key
     * @return
     */

    public static boolean setMapCanRomove(String key)
    {
        try {
            if(isExistMap(key)) {
                return lockMap.put(key,true);
            }
            else {
                return false;
            }
        }
        catch (Exception e)
        {
            return  false;
        }
    }

    /**
     * 移除
     * @param key
     * @return
     */
    public static boolean removeMap(String key)
    {
        try {
            Boolean keyMap=lockMap.get(key);
            boolean bl=false ;
            if(keyMap!=null && keyMap==true) {
                bl = lockList.remove(key);
            }
            log.info("remove"+key+" "+bl);
            return bl;
        }
        catch (Exception e)
        {
            return  false;
        }
    }
}
