package org.kingdom.utils.generator;

import com.baomidou.mybatisplus.annotation.DbType;
import com.baomidou.mybatisplus.annotation.IdType;
import com.baomidou.mybatisplus.generator.AutoGenerator;
import com.baomidou.mybatisplus.generator.config.DataSourceConfig;
import com.baomidou.mybatisplus.generator.config.GlobalConfig;
import com.baomidou.mybatisplus.generator.config.PackageConfig;
import com.baomidou.mybatisplus.generator.config.StrategyConfig;
import com.baomidou.mybatisplus.generator.config.rules.NamingStrategy;

public class Generator {

    /**
     * 设置基础包
     */

    private static String myjweb = "org.kingdom";
    /**
     * 设置表名,逆向工程使用的表
     */

    private static String tableName = "kd_role_premission";

    //数据库驱动
    private static String sqlDriver = "com.mysql.cj.jdbc.Driver";
    private static String sqlUrl = "jdbc:mysql://116.62.247.145:3306/kingdom?serverTimezone=GMT%2B8&useUnicode=true&useSSL=false&characterEncoding=utf8";
    private static String sqlUsername = "root";
    private static String sqlPassword = "kingdom112233+";


    public static void main(String[] args) {
        // 代码生成器
        AutoGenerator mpg = new AutoGenerator();

        // 全局配置
        GlobalConfig gc = new GlobalConfig();
        String projectPath = System.getProperty("user.dir");
        gc.setOutputDir(projectPath + "/src/main/java")    ////生成路径
                .setAuthor("书小页")    ////设置作者
                //.setActiveRecord(true)	//是否使用AR模式 
                .setIdType(IdType.AUTO)        //设置主键自增长策略
                .setFileOverride(true)        //第二次生成会把第一次生成的覆盖掉
                .setBaseResultMap(true)        //生成resultMap
                .setBaseColumnList(true)    //在xml中生成基础列
                .setEnableCache(true)        //开启二级缓存
                .setControllerName("%sController")    //设置Controller名称
                .setOpen(false);
        mpg.setGlobalConfig(gc);

        // 数据源配置
        DataSourceConfig dsc = new DataSourceConfig();
        dsc.setDbType(DbType.MYSQL)
                .setUrl(sqlUrl)
                //.setSchemaName("public");
                .setDriverName(sqlDriver)
                .setUsername(sqlUsername)
                .setPassword(sqlPassword);
        mpg.setDataSource(dsc);

        // 策略配置
        StrategyConfig strategy = new StrategyConfig();
        strategy.setCapitalMode(true)        //开启全局大写命名
                .setNaming(NamingStrategy.underline_to_camel)            //数据库表映射到实体的命名策略
                .setColumnNaming(NamingStrategy.underline_to_camel)    //数据库表字段映射到实体的命名策略
                .setEntityLombokModel(true)                                //是否为lombok模型
                .setRestControllerStyle(true)                            //控制器使用 @RestController注解
                .setEntityBuilderModel(true)                            //是否为构建者模型
                // 公共父类
                //.setSuperEntityClass(xydPackage + ".BaseEentity")			//自定义继承的Entity类全称，带包名
                //.setSuperControllerClass(xydPackage + ".BaseController")	//自定义继承的Entity类全称，带包名

                //对哪张表进行代码生成，如果不设置此项，会把连接的数据库中所有表都生成代码
                .setInclude(tableName)
                .setControllerMappingHyphenStyle(true)          //映射请求路径使用驼峰转连字符
                .setTablePrefix("kd_");                            //表前缀  去掉前缀不用   不去：t_user  -->TUser

        mpg.setStrategy(strategy);


        // 包配置
        PackageConfig pc = new PackageConfig();
        pc.setParent(myjweb);        //基本包名

        mpg.setPackageInfo(pc);

        mpg.execute();
    }

    /*
    	<!--逆向工程 start-->
		<dependency>
			<groupId>com.baomidou</groupId>
			<artifactId>mybatis-plus-generator</artifactId>
            <version>${mybatis.plus.version}</version>
		</dependency>

		<dependency>
			<groupId>org.apache.velocity</groupId>
			<artifactId>velocity-engine-core</artifactId>
			<version>2.1</version>
		</dependency>

		<dependency>
			<groupId>org.mybatis.caches</groupId>
			<artifactId>mybatis-ehcache</artifactId>
			<version>1.1.0</version>
		</dependency>
		<!--end-->
    */

}

