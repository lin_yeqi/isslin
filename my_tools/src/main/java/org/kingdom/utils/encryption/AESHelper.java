package org.kingdom.utils.encryption;

import org.apache.commons.lang3.StringUtils;
import org.apache.shiro.codec.Hex;

import javax.crypto.*;
import javax.crypto.spec.SecretKeySpec;
import java.io.UnsupportedEncodingException;
import java.security.InvalidKeyException;
import java.security.NoSuchAlgorithmException;
import java.security.SecureRandom;

/**
 * AES加密解密工具类
 */
public class AESHelper {
    private static final String defaultCharset = "UTF-8";
    private static final String KEY_AES = "AES";
    @SuppressWarnings("unused")
    private static final String KEY = "asdd123456";

    /**
     * 加密
     *
     * @param data 需要加密的内容
     * @param key  加密密码
     * @return
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws UnsupportedEncodingException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
//    public static String encrypt(String data, String key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
//        return doAES(data, key, Cipher.ENCRYPT_MODE);
//    }

    /**
     * 解密
     *
     * @param data 待解密内容
     * @param key  解密密钥
     * @return
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     * @throws UnsupportedEncodingException
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws InvalidKeyException
     */
//    public static String decrypt(String data, String key) throws InvalidKeyException, NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, IllegalBlockSizeException, BadPaddingException {
//        return doAES(data, key, Cipher.DECRYPT_MODE);
//    }

    /**
     * 加密方法
     * @param data  要加密的数据
     * @param key 加密key
     * @return 加密的结果
     * @throws Exception
     */
    public static String encrypt(String data, String key) throws Exception {
        try {
            Cipher cipher = Cipher.getInstance("AES/ECB/ISO10126Padding");//"算法/模式/补码方式"NoPadding PkcsPadding
            int blockSize = cipher.getBlockSize();

            byte[] dataBytes = data.getBytes();
            int plaintextLength = dataBytes.length;
            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }

            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
//            IvParameterSpec ivspec = new IvParameterSpec(key.getBytes());

            cipher.init(Cipher.ENCRYPT_MODE, keyspec);
            byte[] encrypted = cipher.doFinal(plaintext);

            return Hex.encodeToString(encrypted);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 加密方法
     * @param data  要加密的数据
     * @param key 加密key
     * @return 加密的结果
     * @throws Exception
     */
    public static String encryptByInstance(String data, String key, String cipherInputValue) throws Exception {
        try {

            Cipher cipher = Cipher.getInstance(cipherInputValue);//"算法/模式/补码方式"NoPadding PkcsPadding
            int blockSize = cipher.getBlockSize();

            byte[] dataBytes = data.getBytes();
            int plaintextLength = dataBytes.length;
            if (plaintextLength % blockSize != 0) {
                plaintextLength = plaintextLength + (blockSize - (plaintextLength % blockSize));
            }

            byte[] plaintext = new byte[plaintextLength];
            System.arraycopy(dataBytes, 0, plaintext, 0, dataBytes.length);

            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");
//            IvParameterSpec ivspec = new IvParameterSpec(key.getBytes());

            cipher.init(Cipher.ENCRYPT_MODE, keyspec);
            byte[] encrypted = cipher.doFinal(plaintext);

            return Hex.encodeToString(encrypted);

        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 解密方法
     * @param data 要解密的数据
     * @param key  解密key
     * @return 解密的结果
     * @throws Exception
     */
    public static String decrypt(String data, String key) throws Exception {
        try {
            byte[] encrypted1 = Hex.decode(data);

            Cipher cipher = Cipher.getInstance("AES/ECB/ISO10126Padding");
            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");

            cipher.init(Cipher.DECRYPT_MODE, keyspec);

            byte[] original = cipher.doFinal(encrypted1);
            String originalString = new String(original,defaultCharset);
            return originalString;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 解密方法
     * @param data 要解密的数据
     * @param key  解密key
     * @return 解密的结果
     * @throws Exception
     */
    public static String decryptByInstance(String data, String key, String cipherInputValue) throws Exception {
        try {
            byte[] encrypted1 = Hex.decode(data);

            Cipher cipher = Cipher.getInstance(cipherInputValue);
            SecretKeySpec keyspec = new SecretKeySpec(key.getBytes(), "AES");

            cipher.init(Cipher.DECRYPT_MODE, keyspec);

            byte[] original = cipher.doFinal(encrypted1);
            String originalString = new String(original,defaultCharset);
            return originalString;
        } catch (Exception e) {
            e.printStackTrace();
            return null;
        }
    }

    /**
     * 加解密
     *
     * @param data     待处理数据
     * @param key 密钥
     * @param mode     加解密mode
     * @return
     * @throws NoSuchPaddingException
     * @throws NoSuchAlgorithmException
     * @throws UnsupportedEncodingException
     * @throws InvalidKeyException
     * @throws BadPaddingException
     * @throws IllegalBlockSizeException
     */
    private static String doAES(String data, String key, int mode) throws NoSuchAlgorithmException, NoSuchPaddingException, UnsupportedEncodingException, InvalidKeyException, IllegalBlockSizeException, BadPaddingException {

        if (StringUtils.isBlank(data) || StringUtils.isBlank(key)) {
            return null;
        }
        //判断是加密还是解密
        boolean encrypt = mode == Cipher.ENCRYPT_MODE;
        byte[] content;
        //true 加密内容 false 解密内容
        if (encrypt) {
            content = data.getBytes(defaultCharset);
        } else {
            content = parseHexStr2Byte(data);
        }
        //1.构造密钥生成器，指定为AES算法,不区分大小写
        KeyGenerator kgen = KeyGenerator.getInstance(KEY_AES);
        //2.根据ecnodeRules规则初始化密钥生成器
        //生成一个128位的随机源,根据传入的字节数组

        kgen.init(128, new SecureRandom(key.getBytes()));
        SecureRandom random = SecureRandom.getInstance("SHA1PRNG");
        random.setSeed(key.getBytes());
        kgen.init(256, random);
        //3.产生原始对称密钥
        SecretKey secretKey = kgen.generateKey();
        //4.获得原始对称密钥的字节数组
        byte[] enCodeFormat = secretKey.getEncoded();
        //5.根据字节数组生成AES密钥
        SecretKeySpec keySpec = new SecretKeySpec(enCodeFormat, KEY_AES);

        //6.根据指定算法AES自成密码器
        Cipher cipher = Cipher.getInstance(KEY_AES);// 创建密码器
        //7.初始化密码器，第一个参数为加密(Encrypt_mode)或者解密解密(Decrypt_mode)操作，第二个参数为使用的KEY
        cipher.init(mode, keySpec);// 初始化
        byte[] result = cipher.doFinal(content);
        if (encrypt) {
            //将二进制转换成16进制
            return parseByte2HexStr(result);
        } else {
            return new String(result, defaultCharset);
        }
    }

    /**
     * 将二进制转换成16进制
     *
     * @param buf
     * @return
     */
    public static String parseByte2HexStr(byte buf[]) {
        StringBuilder sb = new StringBuilder();
        for (int i = 0; i < buf.length; i++) {
            String hex = Integer.toHexString(buf[i] & 0xFF);
            if (hex.length() == 1) {
                hex = '0' + hex;
            }
            sb.append(hex.toUpperCase());
        }
        return sb.toString();
    }

    /**
     * 将16进制转换为二进制
     *
     * @param hexStr
     * @return
     */
    public static byte[] parseHexStr2Byte(String hexStr) {
        if (hexStr.length() < 1) {
            return null;
        }
        byte[] result = new byte[hexStr.length() / 2];
        for (int i = 0; i < hexStr.length() / 2; i++) {
            int high = Integer.parseInt(hexStr.substring(i * 2, i * 2 + 1), 16);
            int low = Integer.parseInt(hexStr.substring(i * 2 + 1, i * 2 + 2), 16);
            result[i] = (byte) (high * 16 + low);
        }
        return result;
    }

    public static void main(String[] args) throws Exception {

        String encryptStr = encrypt("{\"mobile\":\"18458794212\"}", "!@#$qweasd123");
        System.out.println(encryptStr);

    }
}
