# 启动
要先启动服务器，在启动客户端

## 服务器
执行 **ServiceApplication** 类下的main方法，即可启动服务器
* 接受客户端信息处理类 **NSHandler**

## 客户端
执行 **ClientApplication** 类下的main方法，即可启动客户端
* 配置连接服务器地址在 **NettyClient** 中修改
* 接收服务器信息处理类 **ClientHandler.class**