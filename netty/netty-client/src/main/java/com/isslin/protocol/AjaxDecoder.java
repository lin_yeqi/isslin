package com.isslin.protocol;

import com.alibaba.fastjson2.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;

import java.util.List;


/**
 * 解码器，对入栈的内容解码，编译成对于的对象
 * 解决TCP 粘包、拆包问题
 * <Void> 表示解码状态交给netty管理
 * 需要在客户端配置上创建改解码器
 * 服务器编码 -> 客户端解码
 */
public class AjaxDecoder extends ReplayingDecoder<Void> {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf in, List<Object> list) throws Exception {
        System.out.println("解码器被调用 AjaxDecoder");

        int len = in.readInt();
        byte[] bytes = new byte[len];
        in.readBytes(bytes);

        String msg = new String(bytes);
        System.out.println(msg);
        AjaxProtocol ajaxProtocol = JSONObject.parseObject(msg, AjaxProtocol.class);
        list.add(ajaxProtocol);

    }
}
