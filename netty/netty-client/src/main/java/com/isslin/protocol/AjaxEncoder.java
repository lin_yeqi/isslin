package com.isslin.protocol;

import com.alibaba.fastjson2.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.CharsetUtil;

/**
 * 编码器，对出栈的内容编码
 * 解决TCP 粘包、拆包问题
 * 需要在客户端配置上创建该编码器
 * 客户端编码 -> 服务器解码
 */
public class AjaxEncoder extends MessageToByteEncoder<AjaxProtocol> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, AjaxProtocol ajaxProtocol, ByteBuf buf) throws Exception {
        System.out.println("AjaxEncoder 调用");
        byte[] bytes = JSONObject.toJSONString(ajaxProtocol).getBytes(CharsetUtil.UTF_8);
        int length = JSONObject.toJSONString(ajaxProtocol).getBytes(CharsetUtil.UTF_8).length;
        buf.writeInt(length);
        buf.writeBytes(bytes);

    }
}
