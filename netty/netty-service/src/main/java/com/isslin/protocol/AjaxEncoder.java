package com.isslin.protocol;

import com.alibaba.fastjson2.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.MessageToByteEncoder;
import io.netty.util.CharsetUtil;

public class AjaxEncoder  extends MessageToByteEncoder<AjaxProtocol> {

    @Override
    protected void encode(ChannelHandlerContext channelHandlerContext, AjaxProtocol ajaxProtocol, ByteBuf buf) throws Exception {
        System.out.println("编码器 AjaxEncoder 调用");

        byte[] bytes = JSONObject.toJSONString(ajaxProtocol).getBytes(CharsetUtil.UTF_8);

        buf.writeInt(ajaxProtocol.getLength());
        buf.writeBytes(bytes);

    }
}
