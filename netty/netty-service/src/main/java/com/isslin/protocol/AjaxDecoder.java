package com.isslin.protocol;

import com.alibaba.fastjson2.JSONObject;
import io.netty.buffer.ByteBuf;
import io.netty.channel.ChannelHandlerContext;
import io.netty.handler.codec.ReplayingDecoder;
import io.netty.util.CharsetUtil;

import java.util.List;

public class AjaxDecoder extends ReplayingDecoder<Void> {
    @Override
    protected void decode(ChannelHandlerContext channelHandlerContext, ByteBuf in, List<Object> list) throws Exception {
        System.out.println("解码器被调用 AjaxDecoder");

        int len = in.readInt();
        byte[] bytes = new byte[len];
        in.readBytes(bytes);

        String msg = new String(bytes, CharsetUtil.UTF_8);
        System.out.println(msg);
        AjaxProtocol ajaxProtocol = JSONObject.parseObject(msg, AjaxProtocol.class);
        list.add(ajaxProtocol);

    }
}
