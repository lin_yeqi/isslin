package com.isslin.service.handler;

import com.alibaba.fastjson2.JSONObject;
import io.netty.channel.ChannelHandlerContext;
import io.netty.channel.SimpleChannelInboundHandler;
import org.slf4j.Logger;
import org.slf4j.LoggerFactory;

public class NSHandler extends SimpleChannelInboundHandler<Object> {

    Logger logger = LoggerFactory.getLogger(NSHandler.class);

    int count = 0;

    @Override
    protected void channelRead0(ChannelHandlerContext channelHandlerContext, Object ajax) throws Exception {
        logger.info("接收到客户端数据 \n{}", JSONObject.toJSONString(ajax));
        System.out.println("接受数量：" + (++count));
    }

    @Override
    public void channelActive(ChannelHandlerContext ctx) throws Exception {
        logger.info("有新的客户端连接");
        logger.info("client ip {}", ctx.channel().remoteAddress());
    }

    @Override
    public void exceptionCaught(ChannelHandlerContext ctx, Throwable cause) throws Exception {
        logger.error("发生异常");
        logger.error("{}", cause);
        ctx.close();
    }
}
