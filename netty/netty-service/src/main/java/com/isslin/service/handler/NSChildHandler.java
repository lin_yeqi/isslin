package com.isslin.service.handler;

import io.netty.channel.ChannelInitializer;
import io.netty.channel.ChannelPipeline;
import io.netty.channel.socket.SocketChannel;
import io.netty.handler.codec.string.StringDecoder;
import io.netty.handler.codec.string.StringEncoder;
import io.netty.handler.timeout.IdleStateHandler;
import java.util.concurrent.TimeUnit;

public class NSChildHandler extends ChannelInitializer<SocketChannel> {

    @Override
    protected void initChannel(SocketChannel channel) throws Exception {
        ChannelPipeline cp = channel.pipeline();
        cp
                //.addLast(new AjaxDecoder())
                //.addLast(new AjaxEncoder())
                // 心跳空闲时间
                .addLast(new IdleStateHandler(5, 5, 10, TimeUnit.MINUTES))
                .addLast(new StringEncoder())
                .addLast(new StringDecoder())
                .addLast(new NSHandler());
    }
}
