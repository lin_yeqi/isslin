package org.seata.service;

import org.seata.entity.Account;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kingdom
 * @since 2020-10-16
 */
public interface IAccountService extends IService<Account> {

}
