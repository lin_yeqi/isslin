package org.seata.mapper;

import org.seata.entity.Account;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;
import org.apache.ibatis.annotations.Mapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kingdom
 * @since 2020-10-16
 */
@Mapper
public interface AccountMapper extends BaseMapper<Account> {

}
