package org.seata.api.fallback;

import lombok.Setter;
import org.seata.api.MoneyApi;
import org.springframework.stereotype.Component;

@Component
public class MoneyApiFallbackImpl implements MoneyApi {

    @Setter
    private Throwable throwable;

    @Override
    public String feignUpdate(String accountId, String aount) {
        return null;
    }
}
