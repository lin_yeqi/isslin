package org.seata.api.factory;

import feign.hystrix.FallbackFactory;
import org.seata.api.MoneyApi;
import org.seata.api.fallback.MoneyApiFallbackImpl;
import org.springframework.stereotype.Component;

@Component
public class MoneyApiFallbackFactory implements FallbackFactory<MoneyApi> {
    @Override
    public MoneyApi create(Throwable throwable) {
        MoneyApiFallbackImpl fallback = new MoneyApiFallbackImpl();
        fallback.setThrowable(throwable);
        return fallback;
    }
}
