package org.seata.api;

import org.seata.api.factory.MoneyApiFallbackFactory;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.RequestParam;


@FeignClient(contextId = "moneyApi", value = "money", fallbackFactory = MoneyApiFallbackFactory.class)
public interface MoneyApi {

    @PostMapping(value = "/money/feignUpdate")
    String feignUpdate(@RequestParam(name = "accountId") String accountId,
                       @RequestParam(name = "aount") String aount);

}