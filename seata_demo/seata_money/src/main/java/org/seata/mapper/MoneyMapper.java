package org.seata.mapper;

import org.apache.ibatis.annotations.Mapper;
import org.seata.entity.Money;
import com.baomidou.mybatisplus.core.mapper.BaseMapper;

/**
 * <p>
 *  Mapper 接口
 * </p>
 *
 * @author kingdom
 * @since 2020-10-16
 */
@Mapper
public interface MoneyMapper extends BaseMapper<Money> {

}
