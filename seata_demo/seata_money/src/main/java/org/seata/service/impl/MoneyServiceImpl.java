package org.seata.service.impl;

import org.seata.entity.Money;
import org.seata.mapper.MoneyMapper;
import org.seata.service.IMoneyService;
import com.baomidou.mybatisplus.extension.service.impl.ServiceImpl;
import org.springframework.stereotype.Service;

/**
 * <p>
 *  服务实现类
 * </p>
 *
 * @author kingdom
 * @since 2020-10-16
 */
@Service
public class MoneyServiceImpl extends ServiceImpl<MoneyMapper, Money> implements IMoneyService {

}
