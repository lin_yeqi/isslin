package org.seata.service;

import org.seata.entity.Money;
import com.baomidou.mybatisplus.extension.service.IService;

/**
 * <p>
 *  服务类
 * </p>
 *
 * @author kingdom
 * @since 2020-10-16
 */
public interface IMoneyService extends IService<Money> {

}
